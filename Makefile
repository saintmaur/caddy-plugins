VERSION := $(shell git describe --tags 2> /dev/null || echo no-tag)
BRANCH := $(shell git symbolic-ref -q --short HEAD)
COMMIT := $(shell git rev-parse HEAD)

PKG=caddy
INSTALL_DIR ?= ~/go/bin
GO_VERSION := $(shell go version | awk '{print $3}' )
BUILD_TIME := $(shell date '+%Y-%m-%d_%H:%M:%S_%Z')
LDFLAGS := -X main.version=$(VERSION) -X main.commit=$(COMMIT) -X main.buildTime=$(BUILD_TIME)

docker:
	@docker build --force-rm --build-arg PLUGIN_NAME="$(target)" -t saintmaur/caddy_$(target):$(VERSION) .
	@docker push saintmaur/caddy_$(target):$(VERSION)

docker_all:
	@target=buffered_balance make docker

build:
	@echo "Build '$(target)'..."
	@cd $(target) && ./build.sh $(INSTALL_DIR)
	@echo "------------------"

build_all:
	@target=buffered_balance make build
	@echo "Build is finished!"

coverage:
	@go clean -testcache
	@go test -coverprofile=coverage.out ./$(target)/...
	@go tool cover -html=coverage.out

test:
	@go clean -testcache
	@go test -cover ./$(target)/...

test_all:
	@target=buffered_balance make test
	@echo "Test is finished!"

race:
	@go test -race ./$(target)/...