package balance

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"regexp"
	"strconv"
	"time"

	"github.com/caddyserver/caddy/v2"
	"github.com/caddyserver/caddy/v2/caddyconfig"
	"github.com/caddyserver/caddy/v2/caddyconfig/caddyfile"
	"github.com/caddyserver/caddy/v2/caddyconfig/httpcaddyfile"
	"github.com/caddyserver/caddy/v2/modules/caddyhttp"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/thanhpk/randstr"
	"gitlab.com/saintmaur/lib/queue"
	"go.uber.org/zap"
)

type Timeouter struct {
	timer *time.Timer
	stopC chan struct{}
}

var (
	metricHistBuckets       = []float64{0.1, 0.5, 1, 10, 15, 20, 25, 30}
	requestsProcessedMetric = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "balancer_requests_total",
		Help: "The total number of processed requests",
	},
		[]string{"status", "server"})
	bufferSizeMetric = promauto.NewGaugeVec(prometheus.GaugeOpts{
		Name: "balancer_buffer_size",
		Help: "The size of the inner queue",
	},
		[]string{"server"})
	bufferTimeoutMetric = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "balancer_buffer_timeout",
		Help: "The number of requests timed out in buffer",
	},
		[]string{"server"})
	upstreamQuarantinedMetric = promauto.NewGaugeVec(prometheus.GaugeOpts{
		Name: "balancer_upstream_quarantine_total",
		Help: "The number of quarantined upstreams",
	},
		[]string{"server", "upstream"})
	processingDurationMetric = promauto.NewHistogram(prometheus.HistogramOpts{
		Name:    "balancer_processing_duration_seconds",
		Help:    "Duration of the upstream processing time in seconds",
		Buckets: metricHistBuckets,
	})
	fullProcessingDurationMetric = promauto.NewHistogram(prometheus.HistogramOpts{
		Name:    "balancer_full_processing_duration_seconds",
		Help:    "Duration of the full processing time in seconds",
		Buckets: metricHistBuckets,
	})
	limitsMetric = promauto.NewGaugeVec(prometheus.GaugeOpts{
		Name: "balancer_upstream_weight",
		Help: "Upstreams rate limit",
	}, []string{"server", "upstream"})
)

func (t *Timeouter) stop() {
	t.timer.Stop()
	t.stopC <- struct{}{}
}

const (
	targetFormat = "https?://.+"
)

func init() {
	caddy.RegisterModule(Middleware{})
	caddy.RegisterModule(Balancer{})
	caddy.RegisterModule(Quarantine{})
	httpcaddyfile.RegisterHandlerDirective("buffered_balance", parseCaddyfile)
}

type nopCloser struct {
	io.Reader
}

func (nopCloser) Close() error { return nil }

func NopCloser(r io.Reader) io.ReadCloser {
	return nopCloser{r}
}

type BufferedResponse struct {
	response *http.Response
	cancel   context.CancelFunc
}

type BufferedRequest struct {
	r          *http.Request
	processedC chan *BufferedResponse
	intm       time.Time
	t          *Timeouter
}

type Middleware struct {
	buffer        *queue.Queue[BufferedRequest]
	logger        *zap.Logger
	BufferTimeout time.Duration `json:"buffer_timeout,omitempty"`
	balancer      *Balancer
	quarantine    *Quarantine
	StrategyRaw   json.RawMessage `json:"strategy,omitempty" caddy:"namespace=http.handlers.buffered_balance inline_key=strategy"`
	QuarantineRaw json.RawMessage `json:"quarantine,omitempty" caddy:"namespace=http.handlers.buffered_balance inline_key=quarantine"`
	stopC         chan struct{}
}

func (Middleware) CaddyModule() caddy.ModuleInfo {
	return caddy.ModuleInfo{
		ID: "http.handlers.buffered_balance",
		New: func() caddy.Module {
			m := new(Middleware)
			return m
		},
	}
}

func (Balancer) CaddyModule() caddy.ModuleInfo {
	return caddy.ModuleInfo{
		ID: "http.handlers.buffered_balance.strategy",
		New: func() caddy.Module {
			m := new(Balancer)
			return m
		},
	}
}

func (Quarantine) CaddyModule() caddy.ModuleInfo {
	return caddy.ModuleInfo{
		ID: "http.handlers.buffered_balance.quarantine",
		New: func() caddy.Module {
			m := new(Quarantine)
			return m
		},
	}
}

func (m *Middleware) start() {
	go func() {
		panicRecover := func() {
			if r := recover(); r != nil {
				m.logger.Warn("Recovered after panic")
			}
		}
		defer panicRecover()
		for {
			select {
			case <-time.After(time.Second):
				for _, target := range m.balancer.strategy.getTargets() {
					limitsMetric.WithLabelValues(m.balancer.Name, target.address).Set(float64(target.weight))
				}
			case request := <-m.buffer.Data():
				bufferSizeMetric.WithLabelValues(m.balancer.Name).Set(float64(m.buffer.Len()))
				if time.Since(request.intm) >= m.BufferTimeout {
					bufferTimeoutMetric.WithLabelValues(m.balancer.Name).Inc()
					m.buffer.Ack()
					continue
				}
				target := m.balancer.Get()
				if len(target) == 0 {
					m.logger.Debug("Failed to find a ready target, requeue",
						zap.String("server", m.balancer.Name),
						zap.Int("buffer", m.buffer.Len()),
						zap.String("request_id", getRequestID(request.r)),
					)
					m.buffer.Nack()
					continue
				}
				m.buffer.Ack()
				request.t.stop()
				go func(_target string) {
					m.logger.Debug("Found a target",
						zap.String("server", m.balancer.Name),
						zap.String("target", _target),
						zap.String("request_id", getRequestID(request.r)),
					)
					response, cancel, err := m.balancer.RoundTrip(_target, request.r)
					if err != nil {
						m.logger.Warn("Failed on RoundTrip",
							zap.String("server", m.balancer.Name),
							zap.String("request_id", getRequestID(request.r)),
							zap.String("error", err.Error()),
						)
					} else {
						m.logger.Debug("Check whether quaranting is needed", zap.String("server", m.balancer.Name))
						if m.quarantine.isSick(response) || m.balancer.isSick(_target) {
							m.logger.Debug("Quaranting is needed. Set banned",
								zap.String("server", m.balancer.Name),
								zap.String("target", _target),
							)
							m.balancer.setBanned(_target, true)
							upstreamQuarantinedMetric.WithLabelValues(m.balancer.Name, _target).Inc()
							go func() {
								<-time.After(m.quarantine.Timeout)
								upstreamQuarantinedMetric.WithLabelValues(m.balancer.Name, _target).Dec()
								m.logger.Debug("Quaranting is over. Set unbanned",
									zap.String("server", m.balancer.Name),
									zap.String("target", _target))
								m.balancer.reset(_target)
								m.balancer.setBanned(_target, false)
							}()
						}
					}
					request.processedC <- &BufferedResponse{response: response, cancel: cancel}
					m.logger.Info("Finished",
						zap.String("server", m.balancer.Name),
						zap.String("request_id", getRequestID(request.r)),
						zap.Int("buffer", m.buffer.Len()))
				}(target)
			case <-m.stopC:
				m.logger.Info("Stop signal has come. Exit processing goroutine.", zap.String("server", m.balancer.Name))
				return
			}
		}
	}()
}

func (m *Middleware) Provision(ctx caddy.Context) error {
	m.logger = ctx.Logger()
	m.buffer = queue.NewQueue[BufferedRequest]()
	if m.StrategyRaw != nil {
		mod, err := ctx.LoadModule(m, "StrategyRaw")
		if err != nil {
			return fmt.Errorf("loading strategy: %v", err)
		}
		b := mod.(*Balancer)
		m.balancer = NewBalancer(b.Name, b.StrategyName, b.Timeout.String(), b.StategyArgs, b.Targets, m.logger)
	}
	if m.QuarantineRaw != nil {
		mod, err := ctx.LoadModule(m, "QuarantineRaw")
		if err != nil {
			return fmt.Errorf("loading quarantine strategy: %v", err)
		}
		q := mod.(*Quarantine)
		m.quarantine, err = NewQuarantine(q.Reason, q.Timeout.String(), q.Args)
		if err != nil {
			return err
		}
	}
	m.stopC = make(chan struct{})
	m.start()
	return nil
}

func getRequestID(r *http.Request) string {
	var result string
	uuid := caddyhttp.GetVar(r.Context(), "uuid")
	if uuid != nil {
		result = uuid.(fmt.Stringer).String()
	}
	return result
}

func (m *Middleware) ServeHTTP(w http.ResponseWriter, r *http.Request, next caddyhttp.Handler) error {
	m.logger.Info("Start processing",
		zap.String("server", m.balancer.Name),
		zap.String("strategy", m.balancer.StrategyName),
		zap.String("request_id", getRequestID(r)),
	)
	processedC := make(chan *BufferedResponse)
	start := time.Now()
	t := &Timeouter{
		timer: time.NewTimer(m.BufferTimeout),
		stopC: make(chan struct{}),
	}
	m.buffer.Push(BufferedRequest{
		r:          r,
		processedC: processedC,
		intm:       time.Now(),
		t:          t,
	})
	go func() {
		m.logger.Debug("Start waiting for timeout",
			zap.String("server", m.balancer.Name),
			zap.String("request_id", getRequestID(r)),
			zap.Time("start", time.Now()),
			zap.Duration("buffer_timeout", m.BufferTimeout))
		select {
		case <-t.stopC:
			return
		case <-t.timer.C:
			m.logger.Warn("Buffer waiting timed out!",
				zap.String("server", m.balancer.Name),
				zap.String("request_id", getRequestID(r)),
				zap.Time("start", time.Now()),
				zap.Duration("buffer_timeout", m.BufferTimeout))
			processedC <- &BufferedResponse{response: &http.Response{
				StatusCode: http.StatusGatewayTimeout,
				Body:       NopCloser(new(bytes.Buffer)),
			}, cancel: func() {}}
		}
	}()
	result := <-processedC
	defer result.cancel()
	response := result.response
	dur := time.Since(start)
	var body []byte
	var statusCode int
	var err error
	if response == nil {
		statusCode = http.StatusInternalServerError
	} else {
		statusCode = response.StatusCode
		defer response.Body.Close()
		body, err = io.ReadAll(response.Body)
		if err != nil {
			m.logger.Warn(fmt.Sprintf("failed on reading response body: %s", err),
				zap.String("server", m.balancer.Name),
				zap.String("request_id", getRequestID(r)))
			statusCode = http.StatusInternalServerError
		}
		for name, value := range response.Header {
			w.Header().Add(name, value[0])
		}
	}
	w.WriteHeader(statusCode)
	w.Write(body)
	fullDur := time.Since(start)
	requestsProcessedMetric.WithLabelValues(fmt.Sprintf("%d", statusCode), m.balancer.Name).Inc()
	processingDurationMetric.Observe(dur.Seconds())
	fullProcessingDurationMetric.Observe(fullDur.Seconds())
	m.logger.Debug("Request processing finished",
		zap.String("server", m.balancer.Name),
		zap.String("request_id", getRequestID(r)),
		zap.Duration("upstream_duration", dur),
		zap.Duration("full_duration", fullDur),
	)
	return nil
}

func (m *Middleware) UnmarshalCaddyfile(d *caddyfile.Dispenser) error {
	var (
		targets             []string
		balancerName        string
		balanceStrategy     string
		requestTimeout      string
		bufferTimeout       string
		balanceStrategyArgs []string
		quarantineArgs      []string
		quarantineTimeout   string
		quarantineReason    string
	)
	for d.Next() {
		for nesting := d.Nesting(); d.NextBlock(nesting); {
			directive := d.Val()
			switch directive {
			case "name":
				if d.NextArg() {
					balancerName = d.Val()
				}
			case "to":
				targets = d.RemainingArgs()
				if len(targets) == 0 {
					return fmt.Errorf("target host is missing")
				}
				for _, target := range targets {
					ok, err := validFormat(target, targetFormat)
					if err != nil {
						return err
					}
					if !ok {
						return fmt.Errorf("target format is invalid: %s", target)
					}
				}
			case "by":
				args := d.RemainingArgs()
				if len(args) == 0 {
					return fmt.Errorf("by: strategy name and arguments are missing")
				}
				balanceStrategy = args[0]
				if len(args) > 1 {
					balanceStrategyArgs = args[1:]
				}
			case "request_timeout":
				if d.NextArg() {
					requestTimeout = d.Val()
				} else {
					return fmt.Errorf("request timeout value is missing")
				}
			case "buffer_timeout":
				if d.NextArg() {
					bufferTimeout = d.Val()
				} else {
					return fmt.Errorf("buffer timeout value is missing")
				}
			case "quarantine_timeout":
				if d.NextArg() {
					quarantineTimeout = d.Val()
				} else {
					return fmt.Errorf("quarantine timeout value is missing")
				}
			case "quarantine":
				d.Next() // skip the 'by' keyword
				if d.NextArg() {
					quarantineReason = d.Val()
				} else {
					return fmt.Errorf("quarantine reason value is missing")
				}
				quarantineArgs = d.RemainingArgs()
				if len(quarantineArgs) == 0 {
					return fmt.Errorf("quarantine arguments are missing")
				}
			}
		}
	}
	if len(targets) == 0 {
		return fmt.Errorf("`to` directive is missing")
	}
	if len(balanceStrategy) == 0 {
		return fmt.Errorf("`by` directive is missing")
	}
	if len(balancerName) == 0 {
		balancerName = randstr.String(10)
	}
	m.balancer = NewBalancer(balancerName, balanceStrategy, requestTimeout, balanceStrategyArgs, targets, caddy.Log())
	m.StrategyRaw = caddyconfig.JSONModuleObject(m.balancer, "strategy", "strategy", nil)
	var err error
	m.quarantine, err = NewQuarantine(quarantineReason, quarantineTimeout, quarantineArgs)
	if err != nil {
		return err
	}
	m.QuarantineRaw = caddyconfig.JSONModuleObject(m.quarantine, "quarantine", "quarantine", nil)
	m.BufferTimeout = prepareTimeout(bufferTimeout, defaultDuration)
	return nil
}

func (m *Middleware) Cleanup() error {
	m.stopC <- struct{}{}
	return nil
}

func parseCaddyfile(h httpcaddyfile.Helper) (caddyhttp.MiddlewareHandler, error) {
	m := new(Middleware)
	err := m.UnmarshalCaddyfile(h.Dispenser)
	return m, err
}

func prepareInt(s string) (int, error) {
	res, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return 0, fmt.Errorf("failed to parse the input string (%s) as an integer: %s", s, err)
	}
	return int(res), nil
}

func validFormat(str, format string) (bool, error) {
	re, err := regexp.Compile(format)
	if err != nil {
		return false, fmt.Errorf("failed on compiling the regexp: %s", format)
	}
	return re.MatchString(str), nil
}

var (
	_ caddy.Provisioner           = (*Middleware)(nil)
	_ caddy.CleanerUpper          = (*Middleware)(nil)
	_ caddyhttp.MiddlewareHandler = (*Middleware)(nil)
	_ caddyfile.Unmarshaler       = (*Middleware)(nil)
	_ caddy.Module                = (*Middleware)(nil)
)
