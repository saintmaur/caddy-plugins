package balance

import (
	"context"
	"crypto/tls"
	"fmt"
	"net/http"
	"net/url"
	"time"

	"go.uber.org/zap"
)

type ContextKey string

const ContextKeyDuration ContextKey = "duration"

type Target struct {
	weight  int
	address string
	locked  bool
	banned  bool
}
type TargetMap map[string]*Target

func (t *Target) String() string {
	return fmt.Sprintf("locked: %t, banned: %t, weight: %d", t.locked, t.banned, t.weight)
}

func (t *Target) setLocked(v bool) {
	t.locked = v
}

func (t *Target) setWeight(v int) {
	t.weight = v
}

func (t *Target) setBanned(v bool) {
	t.banned = v
}

type BalanceStrategy interface {
	getTargets() TargetMap
	getNext() string
	preProcess(string, *http.Request)
	postProcess(string, *http.Response)
	setBanned(string, bool)
	getBanned(string) bool
	isSick(string) bool
	reset(string)
}

type Balancer struct {
	Targets      []string      `json:"targets,omitempty"`
	Timeout      time.Duration `json:"timeout,omitempty"`
	StategyArgs  []string      `json:"strategy_arguments,omitempty"`
	StrategyName string        `json:"strategy_name,omitempty"`
	Name         string        `json:"name,omitempty"`
	logger       *zap.Logger
	strategy     BalanceStrategy
	client       *http.Client
}

const (
	strategy_response_header = "response_header"
	strategy_round_robin     = "round_robin"
	strategy_random          = "random"
	strategy_hash            = "hash"

	defaultDuration = time.Second * 10
)

func NewStrategy(name, strategy string, args []string, logger *zap.Logger) BalanceStrategy {
	var s BalanceStrategy
	logger.Sugar().Debug(fmt.Sprintf("Create the strategy '%s': %v", strategy, args), zap.String("server", name))
	switch strategy {
	case strategy_response_header:
		s = NewResponseHeaderStrategy(args, logger)
	case strategy_round_robin:
		s = NewRoundRobinStrategy(args, logger)
	}
	return s
}

func NewBalancer(balancerName, strategy, timeout string, args []string, proxy_list []string, logger *zap.Logger) *Balancer {
	tr := http.Transport{TLSClientConfig: &tls.Config{InsecureSkipVerify: true}}
	b := &Balancer{
		StrategyName: strategy,
		Name:         balancerName,
		strategy:     NewStrategy(balancerName, strategy, append(args, proxy_list...), logger),
		client:       &http.Client{Transport: &tr},
		Timeout:      prepareTimeout(timeout, defaultDuration),
		Targets:      proxy_list,
		StategyArgs:  args,
		logger:       logger,
	}
	return b
}

func prepareTimeout(timeout string, def time.Duration) time.Duration {
	d, err := time.ParseDuration(timeout)
	if err != nil {
		d = def
	}
	return d
}

func (b *Balancer) Get() string {
	return b.strategy.getNext()
}

func (b *Balancer) setBanned(target string, value bool) {
	b.strategy.setBanned(target, value)
}

func (b *Balancer) getBanned(target string) bool {
	return b.strategy.getBanned(target)
}

func (b *Balancer) reset(target string) {
	b.strategy.reset(target)
}

func (b *Balancer) RoundTrip(target string, req *http.Request) (*http.Response, context.CancelFunc, error) {
	var resp *http.Response
	var err error
	b.strategy.preProcess(target, req)
	defer func() {
		b.strategy.postProcess(target, resp)
	}()
	ctx, cancel := context.WithTimeout(context.Background(), b.Timeout)
	req = req.WithContext(ctx)
	rawQuery := ""
	if len(req.URL.RawQuery) > 0 {
		rawQuery = "?" + req.URL.RawQuery
	}
	u, err := url.Parse(fmt.Sprintf("%s%s%s", target, req.URL.Path, rawQuery))
	if err != nil {
		return nil, cancel, err
	}
	req.RequestURI = ""
	req.URL = u
	req.Host = u.Host
	req.Close = true
	b.logger.Debug("Send a request",
		zap.String("server", b.Name),
		zap.String("request_id", getRequestID(req)),
		zap.String("target", target),
	)
	start := time.Now()
	resp, err = b.client.Do(req)
	if err != nil {
		return nil, cancel, err
	}
	resp.Header.Add("target", target)
	resp.Request = resp.Request.WithContext(context.WithValue(resp.Request.Context(), ContextKeyDuration, time.Since(start)))
	return resp, cancel, err
}

func (b *Balancer) isSick(t string) bool {
	return b.strategy.isSick(t)
}
