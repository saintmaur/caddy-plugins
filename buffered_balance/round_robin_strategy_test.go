package balance

import (
	"testing"
)

func TestGetNextRR(t *testing.T) {
	args := []string{"one", "two", "three"}
	s := NewRoundRobinStrategy(args, lggr)
	if s == nil {
		t.Fatal("Failed to create the strategy")
	}
	for _, target := range args {
		addr := s.getNext()
		if addr != target {
			t.Fatal("Addresses are distinct")
		}
	}
	if args[0] != s.getNext() {
		t.Fatal("Failed to switch the counter")
	}
}

func TestGetNextRRBanned(t *testing.T) {
	args := []string{"one", "two", "three"}
	s := NewRoundRobinStrategy(args, lggr)
	if s == nil {
		t.Fatal("Failed to create the strategy")
	}
	s.setBanned(args[1], true)
	target := s.getNext()
	if args[0] != target {
		t.Fatal("Should switch to this target")
	}
	target = s.getNext()
	if args[1] == target {
		t.Fatal("Should switch to another target")
	}
	if args[2] != target {
		t.Fatal("Should switch to this target")
	}
}
