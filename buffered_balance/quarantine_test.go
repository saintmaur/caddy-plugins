package balance

import (
	"net/http"
	"testing"
)

func TestQuarantingSick(t *testing.T) {
	input := []struct {
		name             string
		args             []string
		r                *http.Response
		shouldFail       bool
		shouldQuarantine bool
	}{
		{
			name: quarantineTypeStatus,
			args: []string{"512"},
			r: &http.Response{
				StatusCode: 512,
			},
			shouldFail:       false,
			shouldQuarantine: true,
		},
		{
			name: quarantineTypeStatus,
			args: []string{"511"},
			r: &http.Response{
				StatusCode: 512,
			},
			shouldFail:       false,
			shouldQuarantine: false,
		},
		{
			name: "",
			args: []string{"512"},
			r: &http.Response{
				StatusCode: 512,
			},
			shouldFail:       true,
			shouldQuarantine: false,
		},
		{
			name: "werty",
			args: []string{"512"},
			r: &http.Response{
				StatusCode: 512,
			},
			shouldFail:       false,
			shouldQuarantine: true,
		},
	}
	for _, d := range input {
		q, err := NewQuarantine(d.name, "1s", d.args)
		if err != nil && !d.shouldFail {
			t.Fatal(err)
		}
		if !d.shouldFail && (!q.isSick(d.r) && d.shouldQuarantine) {
			t.Fatal("quarantine should mark it sick")
		}
	}
}
