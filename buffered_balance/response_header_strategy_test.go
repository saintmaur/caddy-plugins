package balance

import (
	"net/http"
	"testing"
)

func TestGetNextRH(t *testing.T) {
	input := []struct {
		cmpType string
		direct  bool
	}{
		{
			cmpType: "min",
			direct:  true,
		},
		{
			cmpType: "max",
			direct:  false,
		},
		{
			cmpType: "none",
			direct:  true,
		},
	}
	for _, data := range input {
		lggr.Sugar().Infof("Process %s", data.cmpType)
		targets := []string{"two", "three", "four", "five"}
		args := []string{data.cmpType, "header"}
		args = append(args, targets...)
		s := NewResponseHeaderStrategy(args, lggr)
		if s == nil {
			t.Fatal("Failed to create the strategy")
		}
		reverse := func(input []string) []string {
			var output []string
			ln := len(input)
			for i := range input {
				output = append(output, input[ln-i-1])
			}
			return output
		}
		for i, target := range targets {
			s.targets[target].setWeight(i + 1)
		}
		if !data.direct {
			targets = reverse(targets)
		}
		for _, target := range targets {
			addr := s.getNext()
			if addr != target {
				t.Fatalf("Addresses are distinct: actual: %s <> expected: %s\n", addr, target)
			}
			s.targets[addr].setWeight(s.cmp.EdgeValue())
		}
	}
}
func TestPostProcessRH(t *testing.T) {
	cmpTypes := []string{"min", "max", "none"}
	for _, cmpType := range cmpTypes {
		args := []string{cmpType, "one", "two", "three"}
		s := NewResponseHeaderStrategy(args, lggr)
		if s == nil {
			t.Fatal("Failed to create the strategy")
		}
		addr := s.getNext()
		// no header
		s.postProcess(addr, nil)
		if s.targets[addr].locked {
			t.Fatal("Address should be unlocked")
		}
		if s.targets[addr].weight != s.cmp.InitialValue() {
			t.Fatal("Address should be unweighted")
		}
		// wrong header
		r := &http.Response{
			Header: http.Header{
				"o": []string{"1"},
			},
		}
		s.postProcess(addr, r)
		if s.targets[addr].locked {
			t.Fatal("Address should be unlocked")
		}
		if s.targets[addr].weight != s.cmp.InitialValue() {
			t.Fatal("Address should be unweighted")
		}
		// correct header
		r = &http.Response{
			Header: http.Header{
				"One": []string{"1"},
			},
		}
		s.postProcess(addr, r)
		if s.targets[addr].locked {
			t.Fatal("Address should be unlocked")
		}
		if s.targets[addr].weight == 0 {
			t.Fatal("Address should be weighted")
		}
	}
}
