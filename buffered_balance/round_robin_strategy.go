package balance

import (
	"fmt"
	"net/http"
	"sync"

	"go.uber.org/zap"
)

type RingIndex struct {
	i   int
	max int
}

func (ri *RingIndex) index() int {
	return ri.i
}

func (ri *RingIndex) inc() {
	if ri.i >= ri.max {
		ri.i = -1
	}
	ri.i++
}

func (ri *RingIndex) String() string {
	return fmt.Sprintf("%d", ri.i)
}

type RoundRobinStrategy struct {
	lastUsed *RingIndex
	targets  TargetMap
	mu       sync.Mutex
}

func NewRoundRobinStrategy(args []string, logger *zap.Logger) *RoundRobinStrategy {
	targets := make(map[string]*Target)
	for i, addr := range args {
		t := &Target{
			address: addr,
		}
		targets[fmt.Sprintf("%d", i)] = t
	}
	return &RoundRobinStrategy{
		lastUsed: &RingIndex{
			i:   -1,
			max: len(args) - 1,
		},
		targets: targets,
	}
}

func (s *RoundRobinStrategy) getNext() string {
	s.mu.Lock()
	defer s.mu.Unlock()
	s.lastUsed.inc()
	searchStartIndex := s.lastUsed.index()
	banned := true
	addr := ""
	for banned {
		t := s.getCurrentTarget()
		banned = t.banned
		if !banned {
			addr = t.address
		} else {
			s.lastUsed.inc()
			if s.lastUsed.index() == searchStartIndex {
				break
			}
		}
	}
	return addr
}

func (s *RoundRobinStrategy) getCurrentTarget() *Target {
	return s.targets[s.lastUsed.String()]
}

func (s *RoundRobinStrategy) preProcess(target string, req *http.Request) {
}

func (s *RoundRobinStrategy) postProcess(target string, resp *http.Response) {
}

func (s *RoundRobinStrategy) setBanned(target string, value bool) {
	s.mu.Lock()
	defer s.mu.Unlock()
	for _, t := range s.targets {
		if t.address == target {
			t.setBanned(value)
		}
	}
}

func (s *RoundRobinStrategy) getBanned(target string) bool {
	s.mu.Lock()
	defer s.mu.Unlock()
	for _, t := range s.targets {
		if t.address == target {
			return t.banned
		}
	}
	return false
}

func (s *RoundRobinStrategy) reset(target string) {
}

func (s *RoundRobinStrategy) isSick(target string) bool {
	return false
}

func (s *RoundRobinStrategy) getTargets() TargetMap {
	return s.targets
}
