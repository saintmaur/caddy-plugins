package balance

import (
	"net/http"
	"sync"

	"go.uber.org/zap"
)

type CmpOp interface {
	Compare(int, int) bool
	InitialValue() int
	EdgeValue() int
}

type Lt struct{}
type Gt struct{}

func (op Lt) Compare(a, b int) bool {
	return a < b
}
func (op Lt) InitialValue() int {
	return 0
}
func (op Lt) EdgeValue() int {
	return 1e10
}
func (op Gt) Compare(a, b int) bool {
	return a > b
}
func (op Gt) InitialValue() int {
	return 1e10
}
func (op Gt) EdgeValue() int {
	return 0
}

func NewCmpOp(name string) CmpOp {
	switch name {
	case "min":
		return Lt{}
	case "max":
		return Gt{}
	default:
		return Lt{}
	}
}

type ResponseHeaderStrategy struct {
	headerName string
	targets    TargetMap
	mu         sync.Mutex
	logger     *zap.Logger
	cmp        CmpOp
}

func NewResponseHeaderStrategy(args []string, logger *zap.Logger) *ResponseHeaderStrategy {
	cmpOp := NewCmpOp(args[0])
	targets := make(map[string]*Target)
	for _, addr := range args[2:] {
		t := &Target{
			address: addr,
			weight:  cmpOp.InitialValue(),
		}
		targets[addr] = t
	}
	return &ResponseHeaderStrategy{
		cmp:        cmpOp,
		headerName: args[1],
		targets:    targets,
		logger:     logger,
	}
}

func (s *ResponseHeaderStrategy) getNext() string {
	s.mu.Lock()
	defer s.mu.Unlock()
	targetWeight := s.cmp.EdgeValue()
	address := ""
	for addr, t := range s.targets {
		s.logger.Debug("Check target is ready", zap.String("address", addr), zap.String("state", t.String()))
		if !t.locked && !t.banned && s.cmp.Compare(t.weight, targetWeight) {
			targetWeight = t.weight
			address = addr
		}
	}
	if len(address) > 0 {
		s.targets[address].setLocked(true)
	}
	return address
}

func (s *ResponseHeaderStrategy) preProcess(target string, req *http.Request) {

}
func (s *ResponseHeaderStrategy) postProcess(target string, resp *http.Response) {
	s.mu.Lock()
	defer s.mu.Unlock()
	s.targets[target].setLocked(false)
	if resp == nil {
		s.logger.Info("Response is nil. Skip post processing")
		return
	}
	header := resp.Header.Get(s.headerName)
	if len(header) > 0 {
		r, err := prepareInt(header)
		if err != nil {
			s.logger.Warn(err.Error())
			return
		}
		s.targets[target].setWeight(r)
	} else {
		s.logger.Sugar().Debugf("Empty header: %+v", header)
	}
}

func (s *ResponseHeaderStrategy) setBanned(target string, value bool) {
	s.mu.Lock()
	defer s.mu.Unlock()
	s.targets[target].setBanned(value)
}

func (s *ResponseHeaderStrategy) getBanned(target string) bool {
	s.mu.Lock()
	defer s.mu.Unlock()
	banned := false
	if t, ok := s.targets[target]; ok {
		banned = t.banned
	}
	return banned
}

func (s *ResponseHeaderStrategy) isSick(target string) bool {
	s.mu.Lock()
	defer s.mu.Unlock()
	sick := false
	if t, ok := s.targets[target]; ok && t.weight == s.cmp.EdgeValue() {
		sick = true
	}
	return sick
}

func (s *ResponseHeaderStrategy) reset(target string) {
	s.mu.Lock()
	defer s.mu.Unlock()
	if _, ok := s.targets[target]; ok {
		s.targets[target].weight = s.cmp.InitialValue()
	}
}

func (s *ResponseHeaderStrategy) getTargets() TargetMap {
	return s.targets
}
