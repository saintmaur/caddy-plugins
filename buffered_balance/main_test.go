package balance

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"sync"
	"testing"
	"time"

	"net/http/httptest"

	"github.com/caddyserver/caddy/v2"
	"github.com/caddyserver/caddy/v2/caddyconfig/caddyfile"
	"github.com/caddyserver/caddy/v2/caddyconfig/httpcaddyfile"
	"gitlab.com/saintmaur/lib/logger"
)

var configs = []struct {
	title    string
	cfg      string
	mustFail bool
}{
	{
		title: "correct config",
		cfg: `buffered_balance {
			name "<<<blancer>>>"
			to http://localhost:1212 http://localhost:1213 http://localhost:1214
			by response_header x-mbx
			request_timeout 1s
			buffer_timeout 1s
			quarantine by status_code 418 429
			quarantine_timeout 1s
		}`,
		mustFail: false,
	},
	{
		title: "correct config without name",
		cfg: `buffered_balance {
			name
			to http://localhost:1212 http://localhost:1213 http://localhost:1214
			by response_header x-mbx
			request_timeout 1s
			buffer_timeout 1s
			quarantine by status_code 418 429
			quarantine_timeout 1s
		}`,
		mustFail: false,
	},
	{
		title: "no `to` directive",
		cfg: `buffered_balance {
			by response_header x-mbx
			request_timeout 1s
			buffer_timeout 1s
			quarantine by status_code 418 429
			quarantine_timeout 1s
		}`,
		mustFail: true,
	},
	{
		title: "invalid target format",
		cfg: `buffered_balance {
			to localhost
			by response_header x-mbx
			request_timeout 1s
			buffer_timeout 1s
			quarantine by status_code 418 429
			quarantine_timeout 1s
		}`,
		mustFail: true,
	},
	{
		title: "missing targets",
		cfg: `buffered_balance {
			to
			by response_header x-mbx
			request_timeout 1s
			buffer_timeout 1s
			quarantine by status_code 418 429
			quarantine_timeout 1s
		}`,
		mustFail: true,
	},
	{
		title: "missing `by` directive",
		cfg: `buffered_balance {
			to http://localhost:1212 http://localhost:1213
			request_timeout 1s
			buffer_timeout 1s
			quarantine by status_code 418 429
			quarantine_timeout 1s
		}`,
		mustFail: true,
	},
	{
		title: "missing strategy name",
		cfg: `buffered_balance {
			to http://localhost:1212 http://localhost:1213
			by
			request_timeout 1s
			buffer_timeout 1s
			quarantine by status_code 418 429
			quarantine_timeout 1s
		}`,
		mustFail: true,
	},
	{
		title: "missing request timeout value",
		cfg: `buffered_balance {
			to http://localhost:1212 http://localhost:1213
			by response_header x-mbx
			request_timeout
			buffer_timeout 1s
			quarantine by status_code 418 429
			quarantine_timeout 1s
		}`,
		mustFail: true,
	},
	{
		title: "missing buffer timeout value",
		cfg: `buffered_balance {
			to http://localhost:1212 http://localhost:1213
			by response_header x-mbx
			request_timeout 1s
			buffer_timeout
			quarantine by status_code 418 429
			quarantine_timeout 1s
		}`,
		mustFail: true,
	},
	{
		title: "missing quarantine timeout",
		cfg: `buffered_balance {
			to http://localhost:1212 http://localhost:1213
			by response_header x-mbx
			request_timeout 1s
			buffer_timeout 1s
			quarantine by status_code 418 429
			quarantine_timeout
		}`,
		mustFail: true,
	},
	{
		title: "missing quarantine reason",
		cfg: `buffered_balance {
			to http://localhost:1212 http://localhost:1213
			by response_header x-mbx
			request_timeout 1s
			buffer_timeout 1s
			quarantine by
			quarantine_timeout 1s
		}`,
		mustFail: true,
	},
	{
		title: "missing quarantine arguments",
		cfg: `buffered_balance {
			to http://localhost:1212 http://localhost:1213
			by response_header x-mbx
			request_timeout 1s
			buffer_timeout 1s
			quarantine by status_code
			quarantine_timeout 1s
		}`,
		mustFail: true,
	},
	{
		title: "non-int quarantine status codes",
		cfg: `buffered_balance {
			to http://localhost:1212 http://localhost:1213
			by response_header x-mbx
			request_timeout 1s
			buffer_timeout 1s
			quarantine by status_code qwe
			quarantine_timeout 1s
		}`,
		mustFail: true,
	},
}

func TestValidFormat(t *testing.T) {
	format := "abs.*"
	strs := []string{
		"abs",
		"abse",
	}
	for _, str := range strs {
		ok, err := validFormat(str, format)
		if err != nil {
			t.Fatal(err)
		}
		if !ok {
			t.Fatalf("invalid format: %s:%s", str, format)
		}
	}
}
func TestConfig(t *testing.T) {
	for _, config := range configs {
		logger.Infof("Process case: %s", config.title)
		d := caddyfile.NewTestDispenser(config.cfg)
		d.Next() // need to move from -1 position
		m := new(Middleware)
		err := m.UnmarshalCaddyfile(d.NewFromNextSegment())
		if config.mustFail && err == nil {
			t.Fatal("Should fail")
		} else if !config.mustFail && err != nil {
			t.Fatalf("Failed on parsing config: %s", err)
		}
	}
}
func TestProvision(t *testing.T) {
	cfg := `buffered_balance {
		to http://localhost:1212 http://localhost:1213 http://localhost:1214
		by response_header min x-mbx
		request_timeout 1s
		quarantine by status_code 418 429
		quarantine_timeout 1s
	}`
	d := caddyfile.NewTestDispenser(cfg)
	d.Next() // need to move from -1 position
	m := new(Middleware)
	err := m.UnmarshalCaddyfile(d.NewFromNextSegment())
	if err != nil {
		t.Fatalf("Failed on parsing config: %s", err)
	}
	ctx, _ := caddy.NewContext(caddy.Context{Context: context.Background()})
	if err := m.Provision(ctx); err != nil {
		t.Fatalf("Provision shouldn't fail: %s", err)
	}
	if m.balancer == nil {
		t.Fatal("Balancer shouldn't be nil")
	}
}

func TestProcessing(t *testing.T) {
	bigBuffer := initBuffer(2000000)
	defaultBuffer := []byte("OK")
	var servers = []struct {
		title    string
		ports    []string
		f        func(w http.ResponseWriter, r *http.Request)
		pf       func(w *httptest.ResponseRecorder, m *Middleware)
		statuses []int
	}{
		{
			title:    "Check quarantine",
			ports:    []string{"1216"},
			statuses: []int{418},
			f: func(w http.ResponseWriter, r *http.Request) {
				status, _ := prepareInt(r.Header.Get("status"))
				w.WriteHeader(status)
				w.Write(defaultBuffer)
			},
			pf: func(w *httptest.ResponseRecorder, m *Middleware) {
				if w.Result().StatusCode != 418 {
					t.Fatalf("Failed on request processing, invalid status code: %d", w.Result().StatusCode)
				}
				if !m.balancer.getBanned("http://localhost:1216") {
					t.Fatal("Target should be banned")
				}
			},
		},
		{
			title:    "Check success",
			ports:    []string{"1216"},
			statuses: []int{200},
			f: func(w http.ResponseWriter, r *http.Request) {
				status, _ := prepareInt(r.Header.Get("status"))
				w.WriteHeader(status)
				w.Write(defaultBuffer)
			},
			pf: func(w *httptest.ResponseRecorder, m *Middleware) {
				if w.Result().StatusCode != 200 {
					t.Fatalf("Failed on request processing, invalid status code: %d", w.Result().StatusCode)
				}
				if m.balancer.getBanned("http://localhost:1216") {
					t.Fatal("Target should not be banned")
				}
			},
		},
		{
			title:    "Check big buffer",
			ports:    []string{"1216"},
			statuses: []int{200},
			f: func(w http.ResponseWriter, r *http.Request) {
				status, _ := prepareInt(r.Header.Get("status"))
				w.WriteHeader(status)
				w.Write(bigBuffer)
			},
			pf: func(w *httptest.ResponseRecorder, m *Middleware) {
				if w.Result().StatusCode != 200 {
					t.Fatalf("Failed on request processing, invalid status code: %d", w.Result().StatusCode)
				}
			},
		},
		{
			title:    "Check internal error",
			ports:    []string{"1217"},
			statuses: []int{200},
			f: func(w http.ResponseWriter, r *http.Request) {
				status, _ := prepareInt(r.Header.Get("status"))
				w.WriteHeader(status)
				w.Write(defaultBuffer)
			},
			pf: func(w *httptest.ResponseRecorder, m *Middleware) {
				if w.Result().StatusCode != 500 {
					t.Fatalf("Failed on request processing, invalid status code: %d", w.Result().StatusCode)
				}
			},
		},
		{
			title:    "Check buffer latency",
			ports:    []string{"1216"},
			statuses: []int{504, 200},
			f: func(w http.ResponseWriter, r *http.Request) {
				time.Sleep(3 * time.Second)
				status, _ := prepareInt(r.Header.Get("status"))
				w.WriteHeader(status)
				w.Write(defaultBuffer)
			},
			pf: func(w *httptest.ResponseRecorder, m *Middleware) {
				var body []byte
				w.Body.Read(body)
				status, _ := prepareInt(w.Header().Get("status"))
				if w.Result().StatusCode != status {
					t.Fatalf("Failed on request processing, invalid status code: %d <> %d", w.Result().StatusCode, status)
				}
			},
		},
	}
	cfgs := []string{
		`buffered_balance {
			to http://localhost:1216
			by response_header min x-mbx
			request_timeout 10s
			buffer_timeout 1s
			quarantine by status_code 418 429
			quarantine_timeout 200ms
		}`,
		`buffered_balance {
				to http://localhost:1216
				by round_robin
				request_timeout 10s
				buffer_timeout 1s
				quarantine by status_code 418 429
				quarantine_timeout 1s
		}`,
	}
	for _, cfg := range cfgs {
		h := httpcaddyfile.Helper{
			Dispenser: caddyfile.NewTestDispenser(cfg),
		}
		middlware, err := parseCaddyfile(h)
		if err != nil {
			t.Fatalf("Failed on parsing config: %s", err)
		}
		m := middlware.(*Middleware)
		ctx, _ := caddy.NewContext(caddy.Context{Context: context.Background()})
		if err := m.Provision(ctx); err != nil {
			t.Fatalf("Provision shouldn't fail: %s", err)
		}
		if m.balancer == nil {
			t.Fatal("Balancer shouldn't be nil")
		}
		m.start()
		for _, server := range servers {
			lggr.Sugar().Infof("Process %s\n", server.title)
			for _, port := range server.ports {
				startServer(port, "/", server.f)
			}
			var responses []*httptest.ResponseRecorder
			var mu sync.Mutex
			var wg sync.WaitGroup
			for i := 0; i < len(server.statuses); i++ {
				wg.Add(1)
				num := i
				go func() {
					req, err := http.NewRequest("GET", "http://localhost:2020/", nil)
					if err != nil {
						lggr.Sugar().Errorf("Failed on creating a request: %s", err)
						os.Exit(1)
					}
					req.Header.Set("status", fmt.Sprintf("%d", server.statuses[num]))
					w := httptest.NewRecorder()
					w.Header().Set("status", fmt.Sprintf("%d", server.statuses[num]))
					m.ServeHTTP(w, req, nil)
					mu.Lock()
					defer mu.Unlock()
					responses = append(responses, w)
					wg.Done()
				}()
			}
			wg.Wait()
			for i := 0; i < len(responses); i++ {
				server.pf(responses[i], m)
			}
			for _, port := range server.ports {
				stopServer(port)
			}
		}
		m.Cleanup()
	}
}

func TestUtilsPrepareInt(t *testing.T) {
	data := []struct {
		input  string
		result int
	}{
		{
			input:  "200",
			result: 200,
		},
		{
			input:  "200.2",
			result: 0,
		},
		{
			input:  "string",
			result: 0,
		},
	}
	for _, value := range data {
		if result, err := prepareInt(value.input); result != value.result {
			t.Fatalf("Failed on preparing the int: %s", err)
		}
	}
}
func TestUtilsValidFormat(t *testing.T) {
	data := []struct {
		pattern string
		input   string
		result  bool
	}{
		{
			pattern: "abc.*",
			input:   "abc.*",
			result:  true,
		},
		{
			pattern: `\d.\d`,
			input:   "200.2",
			result:  true,
		},
		{
			pattern: `\d+`,
			input:   "string",
			result:  false,
		},
		{
			pattern: `\y+`,
			input:   "string",
			result:  false,
		},
	}
	for _, value := range data {
		if result, err := validFormat(value.input, value.pattern); result != value.result {
			t.Fatalf("Failed on validating format: %s", err)
		}
	}
}
