package balance

import (
	"fmt"
	"os"
	"testing"

	"gitlab.com/saintmaur/lib/logger"
	"go.uber.org/zap"
)

var lggr *zap.Logger

func TestMain(m *testing.M) {
	defer logger.Stop()
	if !logger.InitLogger("", "info", true) {
		fmt.Println("Failed to init the logger")
		os.Exit(1)
	}
	lggr, _ = zap.NewProduction()
	code := m.Run()
	os.Exit(code)
}
