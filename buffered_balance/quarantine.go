package balance

import (
	"fmt"
	"net/http"
	"strconv"
	"time"
)

type Quarantine struct {
	Timeout  time.Duration `json:"timeout,omitempty"`
	Reason   string        `json:"reason,omitempty"`
	Args     []string      `json:"args,omitempty"`
	strategy QuarantineStrategy
}

type QuarantineStrategy interface {
	isSick(resp *http.Response) bool
}

type QuarantineByStatus struct {
	statuses []int
}

const (
	quarantineTypeStatus = "status_code"
	quarantineTypeTime   = "duration"
)

func newQuarantineByStatus(args []string) (*QuarantineByStatus, error) {
	var statuses []int
	for _, status := range args {
		s, err := strconv.Atoi(status)
		if err != nil {
			return nil, err
		}
		statuses = append(statuses, s)
	}
	return &QuarantineByStatus{statuses: statuses}, nil
}

func NewQuarantineStrategy(reason string, args []string) (QuarantineStrategy, error) {
	var q QuarantineStrategy
	var err error
	switch reason {
	case "":
		return nil, fmt.Errorf("an empty quarantine reason provided")
	case quarantineTypeStatus:
		q, err = newQuarantineByStatus(args)
	default:
		q, err = newQuarantineByStatus(args)
	}
	return q, err
}

func (q *QuarantineByStatus) isSick(resp *http.Response) bool {
	for _, status := range q.statuses {
		if resp.StatusCode == status {
			return true
		}
	}
	return false
}

func NewQuarantine(reason, timeout string, args []string) (*Quarantine, error) {
	strategy, err := NewQuarantineStrategy(reason, args)
	if err != nil {
		return nil, err
	}
	return &Quarantine{
		Reason:   reason,
		Timeout:  prepareTimeout(timeout, defaultDuration),
		Args:     args,
		strategy: strategy,
	}, nil
}

func (q *Quarantine) isSick(resp *http.Response) bool {
	return q.strategy.isSick(resp)
}
