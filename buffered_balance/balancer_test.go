package balance

import (
	crand "crypto/rand"
	"fmt"
	"io"
	"math/rand"
	"net/http"
	"testing"
	"time"

	httpex "gitlab.com/saintmaur/lib/http"
	"gitlab.com/saintmaur/lib/logger"
	"go.step.sm/crypto/randutil"
)

var servers map[string]*httpex.Server

func startServer(port string, path string, f func(w http.ResponseWriter, r *http.Request)) *httpex.Server {
	var s *httpex.Server
	fmt.Printf("start server '%s'\n", port)
	if servers == nil {
		servers = make(map[string]*httpex.Server)
	}
	if _, has := servers[port]; !has {
		s = httpex.New(&httpex.ServerConfig{
			Listen: fmt.Sprintf(":%s", port),
		})
		s.AddHandler(path, f)
		s.Run()
		servers[port] = s
	} else {
		s = servers[port]
	}
	return s
}

func stopServer(port string) {
	if s, ok := servers[port]; ok {
		fmt.Printf("stop server '%s'\n", port)
		s.Stop()
		delete(servers, port)
	}
}

func stopServers() {
	for p := range servers {
		stopServer(p)
	}
}

type StrategyData struct {
	name    string
	args    []string
	targets []string
	ports   []string
	f       func(http.ResponseWriter, *http.Request)
	cf      func(*http.Response)
}

func TestRoundTrip(t *testing.T) {
	rand.NewSource(time.Now().UnixNano())
	headerName := "header-name"
	expectedHeaderValue := fmt.Sprintf("%d", rand.Intn(100))
	randPath, _ := randutil.String(20, "abcdefghijklmnopqrstuvwxyz")
	expectedPath := "/" + randPath
	expectedBody := initBuffer(20000)
	var strategies = []StrategyData{
		{
			name:    "response_header",
			args:    []string{headerName},
			ports:   []string{"1212", "1213"},
			targets: []string{"http://localhost:1212", "http://localhost:1213"},
			f: func(w http.ResponseWriter, r *http.Request) {
				logger.Infof("Handler: path: %s", r.URL.Path)
				w.Header().Add(headerName, expectedHeaderValue)
				w.Header().Add("Path", r.URL.Path)
				w.Write(expectedBody)
			},
			cf: func(resp *http.Response) {
				actualHeaderValue := resp.Header.Get(headerName)
				if actualHeaderValue != expectedHeaderValue {
					t.Fatalf("Headers are distinct: expected: %s <> actual: %s\n", expectedHeaderValue, actualHeaderValue)
				}
				defer resp.Body.Close()
				body, err := io.ReadAll(resp.Body)
				if err != nil {
					t.Fatalf("Failed on reading body: %s", err)
				}
				if string(body) != string(expectedBody) {
					t.Fatal("Body is invalid")
				}
				actualPath := resp.Header.Get("Path")
				if actualPath != expectedPath {
					t.Fatalf("Path differs from expectation: expected: %s, actual: %s", expectedPath, actualPath)
				}
			},
		},
		{
			name:    "round_robin",
			args:    []string{},
			ports:   []string{"1214", "1215"},
			targets: []string{"http://localhost:1214", "http://localhost:1215"},
			f: func(w http.ResponseWriter, r *http.Request) {
				w.Header().Add("Path", r.URL.Path)
				w.Write(expectedBody)
			},
			cf: func(resp *http.Response) {
				defer resp.Body.Close()
				body, err := io.ReadAll(resp.Body)
				if err != nil {
					t.Fatalf("Failed on reading body: %s", err)
				}
				if string(body) != string(expectedBody) {
					t.Fatal("Body is invalid")
				}
				actualPath := resp.Header.Get("Path")
				if actualPath != expectedPath {
					t.Fatalf("Path differs from expectation: expected: %s, actual: %s", expectedPath, actualPath)
				}
			},
		},
	}
	for _, data := range strategies {
		balancer := NewBalancer("balancer name", data.name, "1s", data.args, data.targets, lggr)
		req, err := http.NewRequest("GET", "http://localhost:2020"+expectedPath+"?var=val", nil)
		if err != nil {
			t.Fatalf("Failed on creating a request: %s", err)
		}
		for _, port := range data.ports {
			startServer(port, "/", data.f)
		}
		resp, cancel, err := balancer.RoundTrip(balancer.Get(), req)
		defer cancel()
		if err != nil {
			t.Fatalf("Failed on request: %s", err)
		}
		if resp == nil {
			t.Fatal("Response is nil")
		}
		data.cf(resp)
	}
	stopServers()
}

func initBuffer(size int) []byte {
	buffer := make([]byte, 0, size)
	crand.Read(buffer)
	return buffer
}
