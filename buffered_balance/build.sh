mv ../go.work ../_go.work
time xcaddy build --output ${1:-bin}/caddy --with buffered_balance=$(pwd)
mv ../_go.work ../go.work