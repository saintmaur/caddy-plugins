FROM golang:alpine as build
ARG PLUGIN_NAME=balance
ENV SRC_DIR=/go/src/caddy-plugins
RUN apk add make git g++ libc-dev
RUN mkdir ${SRC_DIR}
COPY . ${SRC_DIR}
WORKDIR ${SRC_DIR}
RUN go install github.com/caddyserver/xcaddy/cmd/xcaddy@latest
RUN target=${PLUGIN_NAME} make build
FROM alpine:latest as final
WORKDIR /etc/caddy
COPY --from=build /root/go/bin/caddy .